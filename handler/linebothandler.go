package handler

import (
	"context"
	"fmt"
	"log"
	"math/rand"
	"scg/repository"
	"strings"

	"github.com/spf13/viper"

	"github.com/labstack/echo"
	"github.com/line/line-bot-sdk-go/linebot"
)

var (
	meal   []string
	kmenu  = []string{"กินไรดี", "ขอเมนู"}
	amenu  = []string{"bot", "บอท"}
	thanks = []string{"เคจ้า", "รับทราบ", "จดๆๆ", "แต้งกิ้วน๊าา"}

	mapkeyword map[string][]string
	menuhelper = "menuhelper"
	addmenu    = "addmenu"
)

type HttpCallbackHandler struct {
	Bot         *linebot.Client
	ServiceInfo string
	ReplyToken  *string
}

func LinebotHandler(e *echo.Echo, linebot *linebot.Client) {
	InitialKeyword()
	e.GET("/", func(c echo.Context) error {
		return c.String(200, "Hi, I am linebot")
	})
	handlers := &HttpCallbackHandler{
		Bot:         linebot,
		ServiceInfo: "test",
	}
	e.POST("/", handlers.Callback)
}

func (handler *HttpCallbackHandler) Callback(c echo.Context) error {
	//initial bot data from db
	var connection repository.DBRepository
	mysqlRepo := repository.MysqlRepository{URL: viper.GetString("db.mysql.url"), Database: viper.GetString("db.mysql.database"), Table: viper.GetString("db.mysql.table.meal")}
	meal = mysqlRepo.FindAllMenu()
	ctx := c.Request().Context()
	if ctx == nil {
		ctx = context.Background()
	}
	events, err := handler.Bot.ParseRequest(c.Request())
	if err != nil {
		if err == linebot.ErrInvalidSignature {
			c.String(400, linebot.ErrInvalidSignature.Error())
		} else {
			c.String(500, "internal server error")
		}
	}

	for _, event := range events {
		handler.ReplyToken = &event.ReplyToken
		clientMessage := ""

		switch event.Type {
		case linebot.EventTypeMessage:
			switch message := event.Message.(type) {
			case *linebot.TextMessage:
				clientMessage = message.Text

				switch getKeyFromMessage(clientMessage, 0, 0) {
				case menuhelper:
					handler.botReplyMessage(randomBotMessage(meal))
				case addmenu:
					mysqlRepo.Table = viper.GetString("db.mysql.table.meal")
					connection = mysqlRepo
					connection.AddMenu(strings.Replace(clientMessage, "bot", "", 1))
					handler.botReplyMessage(randomBotMessage(thanks))
				}
			}
		case linebot.EventTypeJoin:
			handler.botReplyMessage("ดีจ้าาา!! ไม่รู้จะกินอะไรหลอ เดี๋ยวเค้าช่วยคิดเอง ^^")
		}
	}
	return c.JSON(200, "")
}
func InitialKeyword() {
	mapkeyword = map[string][]string{
		menuhelper: kmenu,
		addmenu:    amenu,
	}
}

func getKeyFromMessage(text string, ind, loop int) string {
	if loop == len(mapkeyword) {
		return "default"
	}

	for k, v := range mapkeyword {
		if len(v)-1 < ind {
			loop++
			ind = 0
			continue
		}
		if text == v[ind] || strings.Contains(text, v[ind]) {
			fmt.Println(k)
			return k
		}

		ind++
		return getKeyFromMessage(text, ind, loop)
	}

	return "default"
}

func randomBotMessage(messages []string) string {
	return messages[rand.Intn(len(messages))]
}

func (handler *HttpCallbackHandler) botReplyMessage(message string) {
	_, err := handler.Bot.ReplyMessage(*handler.ReplyToken, linebot.NewTextMessage(message)).Do()
	if err != nil {
		log.Print(err)
	}
}
