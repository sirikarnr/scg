package handler

import (
	"encoding/json"
	"net/http"
	"scg/helper"
	"scg/models"
	"scg/service"
)

const (
	inputtype = "inputtype"
	input     = "input"
	field     = "fields"
	location  = "locationbias"

	resulttypeJSON = "json"
	resulttypeXML  = "xml"
)

func SearchResturantsJson(w http.ResponseWriter, r *http.Request) {
	SearchResturants(resulttypeJSON, w, r)
}

func SearchResturants(resultType string, w http.ResponseWriter, r *http.Request) {
	mparam := make(map[string]interface{})
	getQueryParam(r, 0, mparam)
	inputType := mparam[inputtype].(string)
	placeParam := models.PlaceParam{
		Input:     mparam[input].(string),
		InputType: &inputType,
	}

	searchResp := service.PlaceSearch(resultType, r.URL.RawQuery)

	resp := models.Response{
		Param:  placeParam,
		Result: searchResp,
	}
	b, _ := json.Marshal(resp)

	helper.HandlerResponse(w, b)
}

func getQueryParam(r *http.Request, indParam int, mParam map[string]interface{}) map[string]interface{} {
	paramMap := []string{input, inputtype, field, location}
	fieldname := paramMap[indParam]
	name, ok := r.URL.Query()[fieldname]
	if ok {
		mParam[fieldname] = name[0]
	}
	if indParam != len(r.URL.Query())-1 {
		indParam++
		return getQueryParam(r, indParam, mParam)
	}
	return mParam
}
