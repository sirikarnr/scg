package handler

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"scg/helper"
	"scg/utils"
	"time"
)

var (
	sourceData = []interface{}{"X", 5, 9, 15, 23, "Y", "Z"}
)

type requestBody struct {
	Query []interface{} `json:"q"`
}

func FindArrayHandler(w http.ResponseWriter, r *http.Request) {
	defer timeTrack(time.Now(), "find in array")
	request, _ := ioutil.ReadAll(r.Body)
	mreqbody := requestBody{}
	err := json.Unmarshal(request, &mreqbody)
	if err != nil { // don't forget handle errors
	}
	m := make(map[string]int)
	find(mreqbody.Query, sourceData, 0, 0, m)

	b, _ := json.Marshal(m)

	helper.HandlerResponse(w, b)

}

func find(queryData, sourceData []interface{}, queryIndex, sourceIndex int, result map[string]int) map[string]int {
	if queryIndex == len(queryData) {
		return result
	}
	if sourceIndex == len(sourceData) {
		queryIndex++
		return find(queryData, sourceData, queryIndex, sourceIndex, result)
	}

	if utils.ToString(queryData[queryIndex]) == utils.ToString(sourceData[sourceIndex]) {
		result[fmt.Sprintf("%v", queryData[queryIndex])] = sourceIndex
		queryIndex++
		return find(queryData, sourceData, queryIndex, 0, result)
	}

	sourceIndex++
	return find(queryData, sourceData, queryIndex, sourceIndex, result)
}

func timeTrack(start time.Time, name string) {
	elapsed := time.Since(start)
	log.Printf("%s took %s", name, elapsed/time.Millisecond)
}
