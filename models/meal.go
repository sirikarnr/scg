package models

type Meal struct {
	Title       string
	Description *string
	ID          int
}
