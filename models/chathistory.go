package models

type ChatHistory struct {
	ID       string
	ClientID string
	Message  string
}
