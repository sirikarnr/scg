package models

//Response ... api response
type Response struct {
	Param  PlaceParam       `json:"params"`
	Result PlaceAPIResponse `json:"result"`
}

//PlaceParam ... input param for api response
type PlaceParam struct {
	Input        string  `json:"search_input"`
	InputType    *string `json:"input_type,omitempty"` //textquery or phonenumber
	Fields       *string `json:"fields,omitempty"`
	LocationBias *string `json:"location_bias,omitempty"`
	Key          *string `json:"key,omitempty"`
}

//PlaceAPIResponse ... response from external api (parent node)
type PlaceAPIResponse struct {
	Candidates   *[]Candidates `json:"candidates,omitempty"`
	ErrorMessage *string       `json:"error_message,omitempty"`
	Status       string        `json:"status"`
}

//Candidates ... response from external api (child node)
type Candidates struct {
	FormattedAddress string      `json:"formatted_address"`
	Name             string      `json:"name"`
	OpeningHours     OpeningHour `json:"opening_hours"`
	Photos           []Photo     `json:"photos"`
	Rating           float64     `json:"rating"`
}

//OpeningHour ... response from external api (child node)
type OpeningHour struct {
	OpenNow bool `json:"open_now"`
}

//Photo ... response from external api (child node)
type Photo struct {
	PhotoReference string `json:"photo_reference"`
}
