
# SCG Assignment

## ZF3 Framework(PHP) or Express(Node.JS) or Golang
- Create a new controller called “SCG”
- X, 5, 9, 15, 23, Y, Z - Please create a new function for finding X, Y, Z value
- Please use “Place search|Place API(by Google)” for finding all restaurants in Bangsue area and show result by JSON
- Please create one small project for Line messaging API(Up to you), contain a flow diagram, your code, and database.

## Document
- https://bitbucket.org/sirikarnr/scg/src/master/doc/

## Run
- execute `go run main.go`

## Line messaging API
- Line bot serve menu to solve the problem `What do you want to eat?`

# Flow
https://bitbucket.org/sirikarnr/scg/src/master/asset/dolittle.png

# How to run
- create database https://bitbucket.org/sirikarnr/scg/src/master/asset/dump-mydb-201911101939.sql
- install https://ngrok.com/ and run using `ngrok http 9000` command
- goto line developer console 
    -- create new provider https://developers.line.biz/console/register/provider/ 
    -- create message api channel
    -- enable use webhooks 
    -- change webhook_url to `Forwarding` value from ngrok
- add `CHANNEL_SECRET` and `CHANNEL_TOKEN` to environment variable (value from channel info in line developer console)
- execute `go run main.go` command

# Test result
https://bitbucket.org/sirikarnr/scg/src/master/asset/10893817788653.jpg

