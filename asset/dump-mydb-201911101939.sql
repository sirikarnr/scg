-- MySQL dump 10.13  Distrib 8.0.12, for Win64 (x86_64)
--
-- Host: localhost    Database: mydb
-- ------------------------------------------------------
-- Server version	8.0.18

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8mb4 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `chat_history`
--

DROP TABLE IF EXISTS `chat_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `chat_history` (
  `client_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `message` varchar(4000) COLLATE utf8_unicode_ci NOT NULL,
  `id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `chat_history`
--

LOCK TABLES `chat_history` WRITE;
/*!40000 ALTER TABLE `chat_history` DISABLE KEYS */;
INSERT INTO `chat_history` VALUES ('Ud704834383a7ad360ba50d1b8eb8d866','วุ่นวายนะเจ้','02584980-aabd-4dc3-933c-317ec0908ee9'),('U3c4efba5740e50e8fef4496157564517','กิน','0863ff4e-ccd1-42a0-8717-c8d95b4cab67'),('U3c4efba5740e50e8fef4496157564517','กินไรดี','0e453a9f-d684-4f19-b9c5-022873390414'),('U3c4efba5740e50e8fef4496157564517','Food','14855b49-14e1-4c5e-bc02-600329b10b67'),('U3c4efba5740e50e8fef4496157564517','hi','153523bd-a79f-4a64-b045-b0c41df670e2'),('Ud704834383a7ad360ba50d1b8eb8d866','nooooooooooooooooooooooooooooo','15fa69f0-6746-4d41-8222-bcbf2afb4a70'),('U3c4efba5740e50e8fef4496157564517','กินไรดี','1cf4e737-7f1c-45c4-ae5d-e137ab9b4abd'),('U3c4efba5740e50e8fef4496157564517','food','39b2e748-c3f1-4832-9bed-a31487970a89'),('U3c4efba5740e50e8fef4496157564517','eat','3a9c94c5-91a0-4488-8804-10269b1942c2'),('U6da11cab16664540e9772eef8ba704d9','กำลังสุ่นวายเลย','403a2ff4-1913-4e94-9637-ea4a809552be'),('U3c4efba5740e50e8fef4496157564517','Hi bot','4592ad7f-62ff-4fcb-b29d-ba36c1e40aee'),('U3c4efba5740e50e8fef4496157564517','meal','46dd7d6f-be45-452b-a151-b68b0af31b8e'),('U6da11cab16664540e9772eef8ba704d9','อารายกัน','525cb1d5-382d-4d81-a3ef-348ce168c96a'),('U3c4efba5740e50e8fef4496157564517','Meal','53d42d6b-8ae3-4d66-af03-0afc10ce4c83'),('U3c4efba5740e50e8fef4496157564517','meal','55115022-da09-4e8f-8975-c4931e994bb0'),('Ud704834383a7ad360ba50d1b8eb8d866','ทัก','5e4cb263-8fc0-4403-a8e3-58a29a1492c8'),('U3c4efba5740e50e8fef4496157564517','meal','679aa710-0370-4127-95f5-1770952b28af'),('U3c4efba5740e50e8fef4496157564517','ไป','68d6dae1-9fa1-4c65-ae99-f8745eb5743c'),('U3c4efba5740e50e8fef4496157564517','meal','69abe5b7-e1d8-4742-a8e2-49f65a0b2d66'),('U3c4efba5740e50e8fef4496157564517','กินไร','72f227c2-bdd4-4382-a28c-472510281c1d'),('U3c4efba5740e50e8fef4496157564517','กินไรดี','731d3764-4b10-4fe4-9df9-fdd22e74847f'),('U3c4efba5740e50e8fef4496157564517','food','80ae18b4-c4ae-46ee-98cc-bc8988eb627c'),('Ud704834383a7ad360ba50d1b8eb8d866','บ้าหรอ','84cd0738-409c-4313-bc4f-3b935ba36389'),('U3c4efba5740e50e8fef4496157564517','meal','871bfc9a-981a-402d-b5ea-d86bddbd9543'),('Ud704834383a7ad360ba50d1b8eb8d866','โน้วววววววว','8cd1a4dd-7c8f-43c7-8d6c-db4e5ef1813c'),('U3c4efba5740e50e8fef4496157564517','ไป','9315ee0f-602e-4e76-9e5c-b3ae537d28ab'),('U3c4efba5740e50e8fef4496157564517','กินไร','9873110a-1cbf-4db8-9e51-48a257736416'),('U3c4efba5740e50e8fef4496157564517','meal','9cdcba0d-f7d8-46ee-8236-0023639fb3ea'),('U3c4efba5740e50e8fef4496157564517','meal','a8e65aa9-2fa9-43ca-9074-12db59db2ec3'),('U3c4efba5740e50e8fef4496157564517','Hello','ab4121b0-f517-4c3a-b387-fde7d75ef3db'),('U3c4efba5740e50e8fef4496157564517','meal','af794aee-5878-42a5-bebd-6ca5ed8a6abc'),('U3c4efba5740e50e8fef4496157564517','meal','b26b152c-6f68-4dfb-8df6-6b36ab331e86'),('U3c4efba5740e50e8fef4496157564517','Hi','b87ec8dc-d3b5-4455-97c7-e7a1e2eb1742'),('Ud704834383a7ad360ba50d1b8eb8d866','555555555','c52a3392-fdf4-46cb-99da-b690654797ee'),('U3c4efba5740e50e8fef4496157564517','eat','cae37b3b-ae3b-4269-aec1-f79c41df3eef'),('U3c4efba5740e50e8fef4496157564517','meal','d8faa9d8-369b-45e1-845d-1df647e5dca2'),('U3c4efba5740e50e8fef4496157564517','meal','d91c5dca-5e59-40f6-888c-c6b01f9d80b4'),('U3c4efba5740e50e8fef4496157564517','meal','dd04d532-a69a-4a87-801a-8eb69125cbdd'),('U3c4efba5740e50e8fef4496157564517','Hello','e02bb281-6271-418b-95e4-c412001f01a1'),('U3c4efba5740e50e8fef4496157564517','food','e697a1c1-e389-4874-b155-88cb361b3c44'),('U3c4efba5740e50e8fef4496157564517','ทดสอบ linebot ที่พึ่งเขียนจ้า ','eee24517-962d-4165-8f3c-539611ee8044'),('U3c4efba5740e50e8fef4496157564517','food','f3446f50-22d2-4523-96f2-f21e7a1321af'),('U3c4efba5740e50e8fef4496157564517','meal','f4e02aab-e7a1-41ae-8546-6e629c13f1b9'),('U3c4efba5740e50e8fef4496157564517','eat','fc2b0862-b2a2-48af-b744-0009578622df');
/*!40000 ALTER TABLE `chat_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meal`
--

DROP TABLE IF EXISTS `meal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `meal` (
  `title` varchar(100) DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meal`
--

LOCK TABLES `meal` WRITE;
/*!40000 ALTER TABLE `meal` DISABLE KEYS */;
INSERT INTO `meal` VALUES ('ต้มยำกุ้ง',NULL,1),('ยำรวมมิตร',NULL,2),('กระเพราไก่+ไข่ดาว',NULL,3),('ข้าวผัด',NULL,4),('ผัดเผ็ดหมูสามชั้น',NULL,5),('ไก่นึ่งสมุนไพร',NULL,6),('น้ำพริกหนุ่ม ผักลวก',NULL,7),('หมึกผัดไข่เค็ม',NULL,8),('ยำปลากระป๋อง',NULL,9),('หมูย่างเกลือ',NULL,10),('test',NULL,11),('ข้าวต้ม',NULL,12),('ข้าวมันไก่',NULL,13);
/*!40000 ALTER TABLE `meal` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'mydb'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-11-10 19:39:07
