package utils

import (
	"encoding/json"
	"fmt"
	"strings"
)

func JsonToMap(jsonstr string, model interface{}) {
	mres := make(map[string]interface{})
	json.Unmarshal([]byte(jsonstr), &mres)
	ParseMap(mres, &model)
}

func ParseMap(data map[string]interface{}, to interface{}) {
	b, _ := json.Marshal(data)
	json.Unmarshal(b, &to)
}

func ParseMapList(data []map[string]interface{}, to interface{}) {
	b, _ := json.Marshal(data)
	json.Unmarshal(b, to)
}

func ToString(val interface{}) string {
	return strings.ToLower(fmt.Sprintf("%v", val))
}
