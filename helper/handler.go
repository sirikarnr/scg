package helper

import (
	"fmt"
	"net/http"
)

func HandlerResponse(w http.ResponseWriter, b []byte) {
	w.Header().Set("Content-Type", "application/json")
	fmt.Fprintf(w, string(b))
}
