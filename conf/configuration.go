package conf

import (
	"fmt"
	"strings"

	"github.com/spf13/viper"
)

func GetConfig() {
	viper.SetConfigName("application")
	viper.AddConfigPath("./conf")
	viper.AutomaticEnv()
	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))

	err := viper.ReadInConfig()
	if err != nil {
		panic(fmt.Errorf("get config error: %s", err))
	}
}
