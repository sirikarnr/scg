package connector

import (
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"

	"github.com/spf13/viper"
)

//PlaceAPI ... Connect to external api and return jsonstring response
func PlaceAPI(resultType, query string) (*string, error) {
	url := fmt.Sprintf("%v/%v?%v&key=%v", viper.GetString("place_api"), resultType, query, os.Getenv("API_KEY"))
	resp, err := http.Get(url)
	if err != nil {
		return nil, err
	}
	content, _ := ioutil.ReadAll(resp.Body)
	scontent := string(content)
	if resp.StatusCode == http.StatusOK || resp.StatusCode == http.StatusCreated {
		return &scontent, nil
	}
	return nil, errors.New("not found")
}
