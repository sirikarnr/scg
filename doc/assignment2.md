# Assiignment 2 #

- Problem : Please use `Place search|Place API(by Google)` for finding all restaurants in Bangsue area and show result by JSON

## URL ##
 
|               |                                        |
| ------------- | -------------------------------------- |
| **Method**    | GET                                    |
| **Structure**   | `/assignment2/json?inputtype=textquery&fields=photos,formatted_address,name,opening_hours,rating&input=resterant+in+bangsue` |

## Query Params ##
 
| Field Name       | Type           | Description |
| ---------------- | -------------- | ----------- |
| inputtype        | string         | textquery or phonenumber |
| input            | string         | word need to be search   |

## Success Response ##

```json
{
    "params": {
        "search_input": "resterant in bangSue",
        "input_type": "textquery"
    },
    "result": {
        "candidates": [
            {
                "formatted_address": "9 11 ถนน เทอดดำริห์ แขวง บางซื่อ เขตบางซื่อ กรุงเทพมหานคร 10800 ไทย",
                "name": "Bella Casa",
                "opening_hours": {
                    "open_now": true
                },
                "photos": [
                    {
                        "photo_reference": "CmRZAAAAO0yxhPi7aQQJ7rnJgz3bQdhzNlRrLOZUnjv797PU4dGd8q2XYx7KRKeBj12iO1DCNO6J2n5WH8VevWHdkEKQxgTR7Wpad-eSJK4NAHrw0oToCNh3I7v5arDuxiC28I3wEhDw_drwR791ZIxusE8o9XDgGhQbpCFkUqeczgPFVzjIQAYPdAtz7A"
                    }
                ],
                "rating": 4.2
            }
        ],
        "status": "OK"
    }
}
```

## Error Response ##

```json
{
    "params": {
        "search_input": "resterant in bangsue",
        "input_type": "textquery"
    },
    "result": {
        "candidates": [],
        "error_message": "You have exceeded your daily request quota for this API. If you did not set a custom daily request quota, verify your project has an active billing account: http://g.co/dev/maps-no-account",
        "status": "OVER_QUERY_LIMIT"
    }
}
```