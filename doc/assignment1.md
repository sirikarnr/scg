# Assiignment 1 #

- Problem : X, 5, 9, 15, 23, Y, Z - Please create a new function for finding X, Y, Z value
- Find specific value in sample fixed data array (X, 5, 9, 15, 23, Y, Z) and return index for each value

## URL ##
 
|               |                                        |
| ------------- | -------------------------------------- |
| **Method**    | POST                                   |
| **Structure**   | `/assignment1`                       |

## Data Params ##
 
| Field Name       | Type           | Description |
| ---------------- | -------------- | ----------- |
| q                | string[]       | array of string to find |

## Example ##

```json
{
  "q": ["x", "y", "z"]
}
```

## Response ##

```json
{
    "X": 0,
    "Y": 5,
    "Z": 6
}
```