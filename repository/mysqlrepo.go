package repository

import (
	"database/sql"
	"fmt"
	"log"
	"scg/models"

	_ "github.com/go-sql-driver/mysql"
)

const (
	chatHistory = "chat_history"
)

func (conn MysqlRepository) connect() *sql.DB {
	url := fmt.Sprintf("%v/%v", conn.URL, conn.Database)
	db, err := sql.Open("mysql", url)
	if err != nil {
		fmt.Println(err)
		log.Fatalf("%v connection fail", conn.URL)
	}
	return db
}

func (conn MysqlRepository) AddHistory(chatHistory models.ChatHistory) {
	db := conn.connect()
	defer db.Close()

	stmt, err := db.Prepare(fmt.Sprintf("INSERT INTO %v(id, client_id, message) VALUES(?, ?, ?)", conn.Table))
	if err != nil {
		log.Fatalf("insert data error: %v", err)
	}
	_, err = stmt.Exec(chatHistory.ID, chatHistory.ClientID, chatHistory.Message)
	if err != nil {
		fmt.Println(err)
	}
}
func (conn MysqlRepository) AddMenu(menutitle string) {
	db := conn.connect()
	defer db.Close()

	stmt, err := db.Prepare(fmt.Sprintf("INSERT INTO %v(title) VALUES(?)", conn.Table))
	if err != nil {
		log.Fatalf("insert data error: %v", err)
	}
	_, err = stmt.Exec(menutitle)
	if err != nil {
		fmt.Println(err)
	}
}

func (conn MysqlRepository) FindAllMenu() []string {
	db := conn.connect()
	defer db.Close()
	menus := []string{}
	result, _ := db.Query(fmt.Sprintf("select * from %v", conn.Table))
	for result.Next() {
		var meal models.Meal
		err := result.Scan(&meal.Title, &meal.Description, &meal.ID)
		if err != nil {
			panic(err.Error())
		}
		menus = append(menus, meal.Title)
	}
	return menus
}

func (conn MysqlRepository) Remove() {

}
