package repository

import (
	"database/sql"
	"scg/models"
)

type DBRepository interface {
	AddHistory(chatHistory models.ChatHistory)
	AddMenu(menu string)
	FindAllMenu() []string
	Remove()
}

type MongoRepository struct {
	Database   string
	Collection string
}

type MysqlRepository struct {
	Database   string
	Table      string
	URL        string
	Connection *sql.DB
}
