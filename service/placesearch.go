package service

import (
	"net/http"
	"scg/connector"
	"scg/models"
	"scg/utils"
)

//PlaceSearch ... place search API business layer
func PlaceSearch(resultType, query string) models.PlaceAPIResponse {
	respModel := models.PlaceAPIResponse{}
	resp, err := connector.PlaceAPI(resultType, query)
	if err != nil {
		msgErr := err.Error()
		respModel.ErrorMessage = &msgErr
		respModel.Status = http.StatusText(http.StatusRequestTimeout)
		return respModel
	}
	utils.JsonToMap(*resp, &respModel)
	return respModel
}
