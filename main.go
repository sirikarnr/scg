package main

import (
	"log"
	"net/http"
	"os"
	"scg/conf"
	"scg/handler"

	"github.com/labstack/echo"

	"github.com/line/line-bot-sdk-go/linebot"
)

const (
	serverTypeBot = "bot"
)

func main() {
	conf.GetConfig()
	go startServer(getPort(""))
	startBotServer(getPort(serverTypeBot))
}

func startServer(port string) {
	http.HandleFunc("/assignment1", handler.FindArrayHandler)
	http.HandleFunc("/assignment2/json", handler.SearchResturantsJson)
	http.ListenAndServe(":"+port, nil)
}

func startBotServer(port string) {
	e := echo.New()
	handler.LinebotHandler(e, connectLineBot())
	e.Start(":" + port)
}

func connectLineBot() *linebot.Client {
	bot, err := linebot.New(
		os.Getenv("CHANNEL_SECRET"),
		os.Getenv("CHANNEL_TOKEN"),
	)
	if err != nil {
		log.Fatal(err)
	}
	return bot
}

func getPort(servertype string) string {
	var port = os.Getenv("PORT")
	if port == "" {
		switch servertype {
		case serverTypeBot:
			port = "9000"
		default:
			port = "3000"
		}
	}
	return port
}
